import sys

from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QTextEdit
from PyQt5.QtWidgets import QWidget, QAction, QMessageBox, QCheckBox, QFontDialog
from PyQt5.QtWidgets import QProgressBar, QLabel, QComboBox, QStyleFactory, QColorDialog, QCalendarWidget
from PyQt5.QtGui import QIcon


class Window(QMainWindow):
    '''
        Main window constructor
    '''
    def __init__(self):
        super(Window, self).__init__()
        self.setGeometry(50, 50, 500, 300)
        self.setWindowTitle('PyQT Tuts!')
        self.setWindowIcon(QIcon('02/pythonlogo.png'))
        extractAction = QAction('&Get to the Chopper!', self)
        extractAction.setShortcut('Ctrl+Q')
        extractAction.setStatusTip('Leave the application')
        extractAction.triggered.connect(self.close_application)

        openEditor = QAction("&Editor", self)
        openEditor.setShortcut("Ctrl+E")
        openEditor.setStatusTip('Open Editor')
        openEditor.triggered.connect(self.editor)

        openFile = QAction('&Open File', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Open File')
        # openFile.triggered.connect(self.file_open)

        self.statusBar()

        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('&File')
        fileMenu.addAction(extractAction)

        editorMenu = mainMenu.addMenu("&Editor")
        editorMenu.addAction(openEditor)
        
        fileMenu.addAction(openFile)

        self.home()

    def home(self):
        btn = QPushButton('Quit!', self)
        btn.clicked.connect(self.close_application)
        btn.resize(btn.minimumSizeHint())
        btn.move(100, 100)

        extractAction = QAction(QIcon('02/pythonlogo.png'),
                                'Flee the scene',
                                self)
        extractAction.triggered.connect(self.close_application)

        self.toolBar = self.addToolBar('Extraction')
        self.toolBar.addAction(extractAction)

        self.checkBox = QCheckBox('Enlarge Window', self)
        self.checkBox.move(300, 25)
        self.checkBox.stateChanged.connect(self.enlarge_window)

        self.downloadBtn = QPushButton('Download', self)
        self.downloadBtn.move(200, 120)
        self.downloadBtn.clicked.connect(self.download)

        self.progress = QProgressBar(self)
        self.progress.setGeometry(200, 80, 250, 20)

        self.styleChoice = QLabel('Windows Vista', self)
        self.styleChoice.move(50, 150)

        comboBox = QComboBox(self)
        comboBox.addItem("motif")
        comboBox.addItem("Windows")
        comboBox.addItem("cde")
        comboBox.addItem("Plastique")
        comboBox.addItem("Cleanlooks")
        comboBox.addItem("windowsvista")
        comboBox.move(50, 250)

        comboBox.activated[str].connect(self.style_choice)

        fontChoice = QAction('Font', self)
        fontChoice.triggered.connect(self.font_choice)
        self.toolBar.addAction(fontChoice)

        fontColor = QAction('Font bg Color', self)
        fontColor.triggered.connect(self.color_picker)
        self.toolBar.addAction(fontColor)

        cal = QCalendarWidget(self)
        cal.move(500, 200)
        cal.resize(300, 200)

        self.show()

    def close_application(self):
        choice = QMessageBox.question(self,
                                      'Extract!',
                                      'Get into the chopper?',
                                      QMessageBox.Yes | QMessageBox.No)
        if choice == QMessageBox.Yes:
            print('custom application closing!')
            sys.exit()
        else:
            pass

    def enlarge_window(self, state):
        if state == QtCore.Qt.Checked:
            self.setGeometry(50, 50, 1000, 600)
        else:
            self.setGeometry(50, 50, 500, 300)

    def download(self):
        self.completed = 0

        while self.completed < 100:
            self.completed += 0.00001
            self.progress.setValue(self.completed)

    def style_choice(self, text):
        self.styleChoice.setText(text)
        QApplication.setStyle(QStyleFactory.create(text))

    def font_choice(self):
        font, valid = QFontDialog.getFont()
        if valid:
            self.styleChoice.setFont(font)

    def color_picker(self):
        color = QColorDialog.getColor()
        self.styleChoice.setStyleSheet("QWidget { background-color: %s}" % color.name())

    def editor(self):
        self.textEdit = QTextEdit()
        self.setCentralWidget(self.textEdit)




def run():
    app = QApplication(sys.argv)
    GUI = Window()
    sys.exit(app.exec_())

run()
