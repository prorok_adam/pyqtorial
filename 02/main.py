import sys

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget


class Window(QMainWindow):
    '''
        Main window constructor
    '''
    def __init__(self):
        super(Window, self).__init__()
        self.setGeometry(50, 50, 500, 300)
        self.setWindowTitle('PyQT Tuts!')
        self.setWindowIcon(QIcon('pythonlogo.png'))
        self.show()

app = QApplication(sys.argv)
GUI = Window()
sys.exit(app.exec_())
