import sys

from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QWidget, QAction, QMessageBox
from PyQt5.QtGui import QIcon


class Window(QMainWindow):
    '''
        Main window constructor
    '''
    def __init__(self):
        super(Window, self).__init__()
        self.setGeometry(50, 50, 500, 300)
        self.setWindowTitle('PyQT Tuts!')
        self.setWindowIcon(QIcon('02/pythonlogo.png'))
        extractAction = QAction('&Get to the Chopper!', self)
        extractAction.setShortcut('Ctrl+Q')
        extractAction.setStatusTip('Leave the application')
        extractAction.triggered.connect(self.close_application)

        self.statusBar()

        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('&File')
        fileMenu.addAction(extractAction)

        self.home()

    def home(self):
        btn = QPushButton('Quit!', self)
        btn.clicked.connect(self.close_application)
        btn.resize(btn.minimumSizeHint())
        btn.move(100, 100)

        extractAction = QAction(QIcon('02/pythonlogo.png'),
                                'Flee the scene',
                                self)
        extractAction.triggered.connect(self.close_application)

        self.toolBar = self.addToolBar('Extraction')
        self.toolBar.addAction(extractAction)

        self.show()

    def close_application(self):
        choice = QMessageBox.question(self,
                                    'Extract!',
                                    'Get into the chopper?',
                                    QMessageBox.Yes | QMessageBox.No)
        if choice == QMessageBox.Yes:   
            print('custom application closing!')
            sys.exit()
        else:
            pass


def run():
    app = QApplication(sys.argv)
    GUI = Window()
    sys.exit(app.exec_())

run()
